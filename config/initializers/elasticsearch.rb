# Connect to specific Elasticsearch cluster
ELASTICSEARCH_URL = ENV['ELASTICSEARCH_URL'] || 'http://localhost:9200'

# Print Curl-formatted traces in development
#
if Rails.env.development?
  tracer = ActiveSupport::Logger.new(STDERR)
  tracer.level =  Logger::DEBUG
  tracer.formatter = proc { |s, d, p, m| "\e[2m#{m}\e[0m" + "\n" }
end

Elasticsearch::Model.client = Elasticsearch::Client.new tracer: tracer, host: ELASTICSEARCH_URL
