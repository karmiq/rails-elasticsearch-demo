require 'test_helper'

class SearchControllerTest < ActionController::TestCase
  setup do
    @article = articles(:one)

    Article.__elasticsearch__.import force: true
    Article.__elasticsearch__.refresh_index!
  end

  test "should get index" do
    get :index, q: 'mystring'

    assert_response :success
    assert_not_nil  assigns(:articles)
    assert_equal    2, assigns(:articles).size
  end

end
