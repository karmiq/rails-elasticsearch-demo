require 'test_helper'
require 'rails/performance_test_help'

class SearchTest < ActionDispatch::PerformanceTest
  # Refer to the documentation for all available options
  # self.profile_options = { runs: 5, metrics: [:wall_time, :memory],
  #                          output: 'tmp/performance', formats: [:flat] }
  setup do
    Rails.env = 'development'
  end

  test "search" do
    get '/search?q=twitter'
  end
end
