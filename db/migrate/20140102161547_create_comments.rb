class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text    :body
      t.integer :stars
      t.boolean :pick
      t.string  :user
      t.string  :user_location
      t.references :article, index: true

      t.timestamps
    end
  end
end
