require 'zlib'
require 'yaml'
require 'pry'

gzip      = Zlib::GzipReader.open(File.expand_path('../articles.yml.gz', __FILE__))
documents = YAML.load_documents(gzip.read)
gzip.close

ActiveRecord::Base.logger = ActiveSupport::Logger.new(STDERR)

# Skip callbacks
%w| _touch_callbacks
    _commit_callbacks
    after_add_for_categories
    after_add_for_authorships
    after_add_for_authors
    after_add_for_comments  |.each do |c|
    Article.class.__send__ :define_method, c do; []; end
  end

documents.each do |document|
  article = Article.create! document.slice(:title, :abstract, :content, :url, :shares, :published_on)

  article.categories = document[:categories].map do |d|
    Category.find_or_create_by! title: d
  end

  article.authors = document[:authors].map do |d|
    first_name, last_name = d.split(' ').compact.map(&:strip)
    Author.find_or_create_by! first_name: first_name, last_name: last_name
  end

  document[:comments].each { |d| article.comments.create! d }

  article.save!
end

